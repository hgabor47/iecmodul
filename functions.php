<?php
    $base = "flowdbeditor";
    $host = "localhost";
    $username = "root";
    $passwd = "";
    $link = null;

    function dbopen(){
        global $base;
        global $host;
        global $username;
        global $passwd;
        global $link;
            
        $link = mysqli_connect($host,$username,$passwd,$base) or die("Hiba :".mysqli_connect_error());

        $kod="UTF8";
        mysqli_query($link,"set names '".$kod."'");   // set names 'UTF8' 
    }

    function dbselect($utasitas,$order){  //select * from auto
        if (!isset($_SESSION["username"])){
            echo "no username";
            return;
        }



        global $link;
        $res = "";
        $query = "select * from (".$utasitas.") t where 1=1 order by " . $order ;
        $result = mysqli_query($link,$query);
        try {
            $sor = mysqli_fetch_array($result,MYSQLI_NUM);
            if ($sor==NULL) return "";
            $cnt = count($sor);  // 3
            
            //FEJLEC        
            if ($cnt>0){
                $fields = mysqli_fetch_fields($result);
                for ($i=0; $i < count($fields); $i++) { 
                    $res .=  $fields[ $i ]->name  ."|";
                }
                $res .= "\n";                      //   ID|MARKANEV|TIPUSNEV\n
            }
            while ( $sor != null){
                for ($i=0; $i < $cnt; $i++) { 
                    $res .= $sor[ $i ]."|";
                }
                $res .= "\n";                      //   0|A140|2\n1|W168....
                $sor = mysqli_fetch_array($result,MYSQLI_NUM);
            }
        }  catch (Exception $e){

        }
        return $res;
    }

    function dbclose(){
        global $link;
        mysqli_close($link);
    }

    function dbrun($query){
        global $link;
       
        $result = mysqli_query($link,$query);
        return $result;
    }
?>