<?php

include("functions.php");
session_start();

//print_r($_POST);

if ( isset( $_POST["cmd"]) ) {

    switch ($_POST["cmd"]){
        case "reg":
            reg();
            break;
        case "login":
            login();
            break;
        case "logout":
            logout();
            break;
    }
}

function reg(){
    global $link;
    
    dbopen();
    $u = $_POST["username"];
    $p = mysqli_real_escape_string($link,$_POST['password']);
    $p = password_hash($p, PASSWORD_DEFAULT);

    if ((empty($u)) || empty($p) ) {
        echo "1";
        dbclose();
        return ;
    }

    $res = dbrun('insert into user(login,pwd) values ("'.$u.'","'.$p.'")');
    if (!$res) {
        echo "2";
        dbclose();
        return ;
    }
    echo "0";
    dbclose();
}

function login(){
    global $link;
    
    dbopen();
    $u = $_POST["username"];
    $p = mysqli_real_escape_string($link,$_POST['password']);    

    if ((empty($u)) || empty($p) ) {
        echo "1";
        dbclose();
        return ;
    }

    $result = mysqli_query($link,"select * from user where login='$u'");
    if (mysqli_num_rows($result)<1){
        echo "2";
        dbclose();
        return;
    }

    $sor = mysqli_fetch_array($result,MYSQLI_ASSOC);
    if (!password_verify($p, $sor['pwd'] )){
        echo "3";
        dbclose();
        return;
    }
    $_SESSION["username"] = $u;  //js oldalon LOGGEDINUSER
    echo "0";
}

function logout(){
    session_regenerate_id(true);
    session_destroy();
    echo "0";
}
?>