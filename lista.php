<?php
    include("functions.php");
    session_start();
    $sel="";
    if ($_SERVER["REQUEST_METHOD"] =="POST" ){
        if ( (isset($_POST['select']))  && (isset($_POST['order'])) )
        {
            $sel = $_POST['select'];
            $ord = $_POST['order'];
        }            
    }
    else {
        if ( (isset($_GET['select']))  && (isset($_GET['order'])) )
        {
            $sel = $_GET['select'];
            $ord = $_GET['order'];
        }            
    }

    switch ($sel) {
        case 'SQL_javitasilista':
            $sel='SELECT javitas.id, auto.autonev as autonev_s, marka.name as markanev_s, user.login, user.nev as usernev, ugyfel.nev, irszam.varosnev, ugyfel.irszam, ugyfel.lakcim, javitas.behozta 
                FROM auto , ugyfel , irszam , user , marka, szin, javitas 
                where auto.id=javitas.idauto and marka.id=auto.idmarka and szin.id=javitas.idszin and javitas.iduser=user.id and javitas.idugyfel=ugyfel.id and irszam.irszam=ugyfel.irszam';
            break;       
        case 'SQL_markak':
            $sel='select auto.id, concat(marka.name," ", auto.autonev) as autonev 
                from marka, auto 
                where marka.id=auto.idmarka order by 2';
            break;        
        case 'SQL_szinek':
            $sel='select id,name from szin order by 2';
            break;        
        case 'SQL_ugyfelek':
            $sel = 'select u.id, concat_ws(" ",u.nev, u.irszam, i.varosnev, u.lakcim) as nev from ugyfel u, irszam i where i.irszam=u.irszam order by 2';
            break;
        case 'SQL_javitasedit':
            if(isset($_POST['id'])){
                $sel = 'SELECT javitas.id, idauto, idszin, idugyfel, iduser, behozta 
                FROM javitas 
                where javitas.id='.$_POST['id'];
            }
            break;
        case 'SELECT_termeklista':
            if(isset($_POST['keres'])){
                $keres = $_POST['keres'];
            } else {
                $keres="";
            }            
            $sel='select termek.id,csoportnev,termeknev,ar,db,lefoglalva,fizetve,imagelink,vas.login 
              from csoport,termek 
              left join 
                (select vasarlas.idtermek,vasarlas.login,db,lefoglalva,fizetve from vasarlas,user where vasarlas.fizetve is null and user.login=vasarlas.login and user.login="'.$_SESSION["username"].'") vas 
                on vas.idtermek=termek.id 
            where csoport.id=termek.idcsoport and termeknev like "%'.$keres.'%" ';
            break;
        case 'SELECT_termeklistasajat':
            $sel='select vasarlas.id,csoportnev,termeknev,ar,db,lefoglalva,fizetve,imagelink,vasarlas.login 
              from csoport,termek,vasarlas,user 
              where user.login=vasarlas.login and user.login="'.$_SESSION["username"].'" and vasarlas.idtermek=termek.id and csoport.id=termek.idcsoport and fizetve is null';
            break;
        case 'SELECT_vasarlasaim':  
            $sel='SELECT v.fizetve,count(*) as tetelek,sum(ar) as ar from vasarlas v,termek t where 
            t.id=v.idtermek and v.fizetve is not null and
            v.login="'.$_SESSION["username"].'" 
            group by v.fizetve order by 1 desc';  
            $ord = "1 desc" ;                     
            break;
        case 'SELECT_tetel':
            if(isset($_POST['ido'])){
                $sel='select vasarlas.id,csoportnev,termeknev,ar,db,lefoglalva,fizetve,imagelink,vasarlas.login
                from csoport,termek,vasarlas,user 
                where user.login=vasarlas.login and user.login="'.$_SESSION["username"].'" and vasarlas.idtermek=termek.id and csoport.id=termek.idcsoport and fizetve="'.$_POST['ido'].'"';
            }
            break;
        case 'SELECT_userinfo':
            $sel = 'select id,login,email from user where login="'.$_SESSION["username"].'"';
            break;
        default:
            $sel="";
            return ;
            break;
    }


    dbopen();
    $res = dbselect($sel,$ord);
    dbclose();
    print($res);
?>