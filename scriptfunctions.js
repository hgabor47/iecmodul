iecmodul='iecmodul/';
loggedinuser="";
function loggedin(){
    var l = sessionStorage.getItem("loggedinuser");
    if ((l!=null) && (l!="")){
        loggedinuser=l;
        return true;
    }
    return false;
}


function reg(DOMREGDIV,DOMusername,DOMjelszo1,DOMjelszo2){
    var login=document.getElementById(DOMusername);
    var pwd1=document.getElementById(DOMjelszo1);
    var pwd2=document.getElementById(DOMjelszo2);
    if (pwd1.value!=pwd2.value){
        alert("A jelszavak nem egyeznek!");
        return;
    }
    var sikeres = runAuth('reg',login.value,pwd1.value);
    if (sikeres=="0"){
        alert("Sikeres regisztráció");
    } else {
        alert("Nem sikeres sajnos!");
    }
    document.getElementById(DOMREGDIV).style.visibility="hidden";
}

function login(DOMpanelDIV,DOMusername,DOMjelszo){
    var login=document.getElementById(DOMusername);
    var pwd1=document.getElementById(DOMjelszo);
    var sikeres = runAuth('login',login.value,pwd1.value);
    if (sikeres=="0"){
        //alert("Sikeres belépés");
        loggedinuser=login.value; 
        sessionStorage.setItem("loggedinuser",loggedinuser);               
    } else {
        alert("Sikertelen belépés!");
    }
    document.getElementById(DOMpanelDIV).style.visibility="hidden";
    refreshElements();
}
5
function logout() {
    var sikeres = runAuth('logout');
    if (sikeres=="0"){
        alert("Kijelentkezett!");
        loggedinuser="";  
        sessionStorage.setItem("loggedinuser",loggedinuser);               
    }   
    refreshElements();     
}


/*
    aZ ELSŐ oszlop kötelezően ID
    A query mezőnek végén levő _s azt jelenti, hogy lehet rá keresni
    leugromenu-t a táblázat készíti el, azaz alapesetben nincs (null)
    2018.0525: Query átértelmezése: pl: SQL_javitasilista
*/

function tablazat(query,tableid,leugromenu){ // select * from.....  ,  lista    
    var order = 1;
    
    if (leugromenu!=null){
        order = leugromenu.value;        
    }
    load(iecmodul+"lista.php",query, order  ,tableid); //az első sor a fejléc
}

function load(fajl,query,order,tableid){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if (this.readyState==4) { // && xhttp.status==200){
            table = [];  //az első sor a fejléc
            var s = this.responseText; //   0|A140|2\n1|W168....\n
            var sor = s.split("\n");
            for (let i = 0; i < sor.length-1; i++) {
                const element = sor[i];
                var d = element.split("|");
                table.push( d )
            }
            listaz( table,query,order,tableid );
        }
    }
    //GET
    //xhttp.open("GET",fajl+"?select="+select+"&order="+order,true);            
    //xhttp.send();            
    //POST
    xhttp.open("POST",fajl,true);            
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.send("select="+query+"&order="+order);            
}

function runSQL(query,fuggveny,param){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if (this.readyState==4) { // && xhttp.status==200){
            table = [];  //az első sor a fejléc
            var s = this.responseText; //   0|A140|2\n1|W168....\n
            var sor = s.split("\n");
            for (let i = 0; i < sor.length-1; i++) {
                const element = sor[i];
                var d = element.split("|");
                table.push( d )
            }
            fuggveny(table); //1. sora fejlec  2.adat        
        }
    }
    xhttp.open("POST",iecmodul+"lista.php",true);            
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    if (param==null){
        xhttp.send("select="+query+"&order=1");            
    } else {
        xhttp.send("select="+query+"&order=1&"+param);            
    }
}

// tomb[0] = [0]="1",[1]="A 140", [2]..
// tomb[1] = [0]="1",[1]="w168", [2]=4
function listaz( tomb,query,order,tableid ){
    var div = document.getElementById(tableid);
    div.innerHTML="";

    // sorrendi select tag
    var opt = document.createElement("select");
    opt.setAttribute("onchange","tablazat('"+query+"' , '"+ tableid+"',this)")
    div.appendChild(opt);
    for (let i = 0; i < tomb[0].length; i++) {
        const e = tomb[0][i];
        if (e.indexOf("_s")>0){
            var b = document.createElement("option");
            b.setAttribute("value",i);		//Az adott select optionnak lrtlke a mezo sorszama azaz: i
            b.innerHTML=e.replace("_s","");
            if (i==order){
                b.setAttribute("selected","");
            }
            opt.appendChild(b);
        }
    }


    //elso sor fejlec
    var t = document.createElement("table");
    t.className="table";
    div.appendChild(t);
    for (let i = 0; i < tomb.length; i++) {
        const sor = tomb[i];
        var r = document.createElement("tr");
        if (i==0){               
            r.className="fejlec";         
        } 
        t.appendChild(r);
        var id=sor[0];
        for (let j = 1; j < sor.length; j++) { //ID mindig kotelezoen az elso ezert 1 tol
            const cell = sor[j];
            var c= document.createElement("td");
            r.appendChild(c);
            if (i==0){
                c.innerHTML=cell.replace("_s","");
            } else{
                c.innerHTML=cell;
            }
        }         
        if (i>0){ 
            var c= document.createElement("td");
            r.appendChild(c);
            c.innerHTML='<button onclick="adatlap('+id+')">Adatlap</button>';      
        }
    }            
}

// command :   reg, login , logout 
function runAuth(command,username,password){
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST",iecmodul+"auth.php",false);            
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.send("cmd="+command+"&username="+username+"&password="+password); 
    return xhttp.responseText;  //"0" = sikeres .... 1,2,2
}

function runUpdate(command,params){
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST",iecmodul+"update.php",false);            
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    if (params==null){
        xhttp.send("cmd="+command); 
    } else {
        var p="";
        for (let i = 0; i < params.length; i++) {
            p+="&p"+i+'='+params[i];             //.php?cmd=UPDATE_javitas&p0=2018.12.23&p1=2&p2=6&p3=8&p4=1
        }
        xhttp.send("cmd="+command + p ); 
    }
    return xhttp.responseText;  //"0" = sikeres .... 1,2,2
}


function fillCombo(comboDOM,sql,value,fuggveny){
    runSQL(sql,
        function(tomb2){ 
            var dom = document.getElementById(comboDOM);
            dom.innerHTML="";
            for (let i = 1; i < tomb2.length; i++) {                          
                var opt = document.createElement("option");
                opt.value=tomb2[i][0];
                opt.innerHTML=tomb2[i][1];
                if (tomb2[i][0] == value ) {
                    opt.selected = true;
                }
                dom.appendChild(opt);
            }
            if (fuggveny!=null)
                fuggveny();
    });
}

function refreshElements(){
    elements = getAllElementsWithAttributes('protect');
    for (let i = 0; i < elements.length; i++) {
        const e = elements[i];
        
        if (loggedinuser){
            e.style.visibility="visible";
        } else {
            e.style.visibility="hidden";
        }
    }
}

function getAllElementsWithAttributes(attribute){
    var matchinElements = [];
    var allElements = document.getElementsByTagName('*');
    for (let i = 0; i < allElements.length; i++) {
        const element = allElements[i];
        if (element.getAttribute(attribute) != null ){
            matchinElements.push(element);
        }
    }
    return matchinElements;
}

